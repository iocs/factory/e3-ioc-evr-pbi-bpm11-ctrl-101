require essioc
require mrfioc2, 2.3.1+15
require sdsioc, 0.0.1+1

#-All systems will have counters for 14Hz, PMorten and DoD
epicsEnvSet("EVREVTARGS" "N0=F14Hz,E0=14,N1=PMortem,E1=40,N2=DoD,E2=42")
#-BI EVRs should have also BPulseSt, BPulseEnd, BiAcqSt Counters
epicsEnvSet("EVREVTARGS" "$(EVREVTARGS),N3=BPulseSt,E3=12,N4=BPulseEnd,E4=13,N5=BiAcqSt,E5=18")
epicsEnvSet("EVREVTARGS" "$(EVREVTARGS),N6=LEBTCSt,E6=6,N7=LEBTCEnd,E7=7,N8=MEBTCSt,E8=8,N9=MEBTCEnd,E9=9,NA=IonMagSt,EA=10,NB=IonMagEnd,EB=11")

iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=PBI-BPM11:Ctrl-EVR-101,PCIID=0e:00.0,EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=PBI-BPM11:Ctrl-EVR-101"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=PBI-BPM11:Ctrl-EVR-101,$(EVREVTARGS=)"

afterInit('iocshLoad($(mrfioc2_DIR)/evr.r.iocsh                   "P=PBI-BPM11:Ctrl-EVR-101, INTREF=#")')
afterInit('iocshLoad($(mrfioc2_DIR)/evrtclk.r.iocsh               "P=PBI-BPM11:Ctrl-EVR-101")')







dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-0-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-0-Evt-Trig0-SP, VAL=18"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-1-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-1-Evt-Trig0-SP, VAL=12"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-2-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-2-Evt-Trig0-SP, VAL=13"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-3-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-3-Evt-Trig0-SP, VAL=13"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-4-Delay-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-4-Evt-Trig0-SP, VAL=125"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-5-Delay-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-5-Evt-Trig0-SP, VAL=40"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-6-Delay-SP, VAL=1"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-6-Evt-Trig0-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-7-Delay-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-7-Evt-Trig0-SP, VAL=42"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-8-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-8-Evt-Trig0-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-9-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-9-Evt-Trig0-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-10-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-10-Evt-Trig0-SP, VAL=7"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-11-Delay-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-11-Evt-Trig0-SP, VAL=6"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-12-Delay-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-12-Evt-Trig0-SP, VAL=8"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:DlyGen-12-Evt-Trig1-SP, VAL=9"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-Back0-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-Back1-Src-SP, VAL=1"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-Back2-Src-SP, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-Back3-Src-SP, VAL=4"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-Back4-Src-SP, VAL=5"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-Back6-Src-SP, VAL=7"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-FP0-Src-SP, VAL=53"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:Out-FPUV0-Src-SP, VAL=52"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:BPIn-5-Code-Ext-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=PBI-BPM11:Ctrl-EVR-101:BPIn-7-Code-Ext-SP, VAL=43"




#- Fixing IdCycle to 64bits
dbLoadRecords "cycleid-64bits.template"               "P=PBI-BPM11:Ctrl-EVR-101:"
#- Fixing databuffer Information
dbLoadRecords "BDest.template" "P=PBI-BPM11:Ctrl-EVR-101:,PV=BDest-I"
dbLoadRecords "BMod.template" "P=PBI-BPM11:Ctrl-EVR-101:,PV=BMod-I"
dbLoadRecords "BPresent.template" "P=PBI-BPM11:Ctrl-EVR-101:,PV=BPresent-I"
#- Configuring PVs to be archived
iocshLoad("archive-default.iocsh","P=PBI-BPM11:Ctrl-EVR-101:")


#- ----------------------------------------------------------------------------
#- SDS Metadata Capture
#- ----------------------------------------------------------------------------
iocshLoad("$(sdsioc_DIR)/sdsCreateMetadataEVR.iocsh","PEVR=PBI-BPM11:Ctrl-EVR-101:,F14Hz=F14Hz")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

